import imgKing from "../public/asset/king.jpg";
import imgDrgNoir from "../public/asset/dragonnoir.webp";
import imgherbe from "../public/asset/herbmiel.jpg";
import imgJumelle from "../public/asset/hinoaminoto.jpg";
import imgPaw from "../public/asset/paws.webp";
import imgPet from "../public/asset/pet.jpg";
import imgRise from "../public/asset/risesunbreak.jpg";
import imgTetra from "../public/asset/tetranodon.webp";
import imgTrois from "../public/asset/trois.jpg";
import imgAlatreon from "../public/asset/alatreon.webp";
import imgFata from "../public/asset/fata.gif";
import imgkelbi from "../public/asset/kelbi.gif";
import imghammer from "../public/asset/hammer.gif";
import imgrajang from "../public/asset/rajang.gif";

const timerElement = document.querySelector<HTMLElement>('.timer');
const start = document.querySelector<HTMLButtonElement>('.start');
const restart = document.querySelector<HTMLButtonElement>('.restart');
const exitButton = document.querySelector<HTMLButtonElement>('.quit');
const NextBtn = document.querySelector<HTMLButtonElement>('.next_btn');
const resultBtn = document.querySelector<HTMLButtonElement>('.result_button');
const resultImg = document.querySelector<HTMLImageElement>('.resultImg');
const questionImg = document.querySelector<HTMLImageElement>('.question_img');

const info_box = document.querySelector<HTMLElement>('.info_box');
const quiz_box = document.querySelector<HTMLElement>('.quiz_box');
const result_box = document.querySelector<HTMLElement>('.result_box');
const scoreElement = document.querySelector<HTMLElement>('.score');
const scoreResult = document.querySelector<HTMLElement>('.score_text');
const finalText = document.querySelector<HTMLElement>('.final_text');

const img = document.querySelector("img");
const timeMaxSecond = 15;
let intervalId: number;

let timeCurrentSecond = timeMaxSecond;
let score: number = 0;
let questionCurrent = 0;

//Cette fonction permet de cacher toutes les boites//
function timer() {
  if (timerElement) timerElement.innerHTML = String(timeMaxSecond);
  intervalId = setInterval(() => {
    timeCurrentSecond--;
    if (timerElement) timerElement.innerHTML = String(timeCurrentSecond);
    if (timeCurrentSecond === 0) {
      validateAnswer();
      clearInterval(intervalId);
    }
  }, 1000);
}

function HideAllBox() {
  if (info_box) {
    info_box.style.display = 'none';
  }
  if (quiz_box) {
    quiz_box.style.display = 'none';
  }
  if (result_box) {
    result_box.style.display = 'none';
  }
}

/*function qui montre infobox et cache les autres*/
function ShowInfoBox() {
  HideAllBox();
  if (info_box) info_box.style.display = 'flex';
}

/*function qui montre le quiz et cache les autres*/
function ShowQuizzBox() {
  HideAllBox();
  if (quiz_box) quiz_box.style.display = 'flex';
}

/*function qui montre la boxe de score et cache les autres*/
function ShowResultBox() {
  HideAllBox();
  if (result_box) result_box.style.display = 'flex';
  if (scoreResult) scoreResult.innerHTML = String(score);
  if (finalText) {
    if (score <= 10) {
      finalText.innerText = "Tu es juste bon à taper un kelbi !";
      if (resultImg) resultImg.src = imgkelbi;
      ;
    }
    else if (score <= 30) {
      finalText.innerText = "Pas mal, pas mal... retourne à l'entrainement !";
      if (resultImg) resultImg.src = imghammer;
    }
    else if (score <= 40) {
      finalText.innerText = "Génial continue comme ça !";
      if (resultImg) resultImg.src = imgrajang;
    }
    else if (score <= 50) {
      finalText.innerText = "Tu est prêt à affronter le Fatalis !";
      if (resultImg) resultImg.src = imgFata;
    }
    else {
      finalText.innerText = "Tu es juste bon à taper un kelbi !";
    }
  }
}

/*bouton start: quand on clique cela fait appraitre la boxe question*/
start?.addEventListener('click', () => {
  ShowQuizzBox();
  loadQuestion(questions[0]);
})

/*bouton prochaine question: quand on clique cela fait appraitre la prochaine question*/
NextBtn?.addEventListener('click', () => {
  (questionCurrent == questionCurrent++)
  loadQuestion(questions[questionCurrent])

  if (questionCurrent === questions.length - 1) { // On est en train de charger la dernière question
    if (NextBtn) {
      NextBtn.classList.add('hidden');
    }
    if (resultBtn) {
      resultBtn.classList.remove('hidden');
    }
  }
})

resultBtn?.addEventListener('click', () => {
  ShowResultBox();
})

//Bouton exit: nous ramène à la box infos//
exitButton?.addEventListener('click', () => {
  reset();
  ShowInfoBox();
})

restart?.addEventListener('click', () => {
  reset();
  loadQuestion(questions[0]);
  ShowQuizzBox();
})


function reset() {
  // on reset le score
  score = 0;
  if (scoreElement) { scoreElement.innerText = String(score); }

  //On reset questionCurrent
  questionCurrent = 0;

  // On reset les styles des réponses
  for (let i = 0; i < 4; i++) {
    const option = document.querySelector<HTMLElement>('#option' + i);
    option?.classList.remove("right", "wrong");
  }

  // reset les boutons nextbtn et resultbtn
  if (NextBtn) {
    NextBtn?.classList.remove("hidden");
    NextBtn.disabled = false;
  }
  if (resultBtn) {
    resultBtn?.classList.add("hidden");
    resultBtn.disabled = true;
  }

}

/*function qui gère le clic sur les réponses*/
function validateAnswer(optionElement:EventTarget | null = null) {
  if (optionElement !== null) {
    if (optionElement.classList.contains("goodAnswer")) { // Bonne réponse

      score += Math.round(timeCurrentSecond / 3);

      if (scoreElement) { scoreElement.innerText = String(score); }
    }
    else { // mauvaise réponse
      optionElement?.classList.add("wrong");  //Problème TS non résolu à trois endroits (pas trouvé comment corrigé cela)
    }
  }
  const good = document.querySelector<HTMLElement>('.goodAnswer');
  if (good) {
    good?.classList.add("right");
  }

  if (questionCurrent === questions.length - 1) { // On est en train de valider la derniere réponse
    if (resultBtn) {
      resultBtn.disabled = false;
    }
  } else {
    if (NextBtn) {
      NextBtn.disabled = false;
    }
  }
  for (let i = 0; i < 4; i++) {
    const option = document.querySelector<HTMLElement>('#option' + i);
    option?.classList.add("disabled");
  }

  timeCurrentSecond = timeMaxSecond;
}

for (let i = 0; i < 4; i++) {
  const option = document.querySelector<HTMLElement>('#option' + i);
  option?.addEventListener('click', (event) => {
    const target = event.target;
    if (target) {
      if (!target.classList.contains("disabled")) { // L'élément n'est pas disabled
        clearInterval(intervalId);
        validateAnswer(target);
      }
    }
  })
}

/*function qui charge les questions dans l'interface*/
function loadQuestion(question:any) {
  if (NextBtn) {
    NextBtn.disabled = true;
  }
  if (question.imgSrc) {
    if (questionImg) {
      questionImg.src = question.imgSrc;
      questionImg.classList.remove("hidden");
    }
  } else {
    if (questionImg) questionImg.classList.add("hidden");
  }

  for (let i = 0; i < 4; i++) {
    const option = document.querySelector<HTMLElement>('#option' + i);
    option?.classList.remove("disabled");
  }
  const qts = document.querySelector<HTMLElement>('.question_text');
  if (qts) { qts.innerText = question.label; }


  for (var i = 0; i < question.options.length; i++) {
    let rep;
    rep = document.querySelector<HTMLElement>('#option' + i);

    if (rep) { rep.innerText = question.options[i]; }

    // on réinitialise chaque réponse
    rep?.classList.remove("goodAnswer", "right", "wrong");

    if (i === question.goodAnswer) {
      rep?.classList.add("goodAnswer");
    }
  }

  timer();
}

//Liste des questions//

let questions = [
  {
    label: "Qui est surnommé le roi des cieux?",
    imgSrc: imgKing,
    goodAnswer: 2,
    options: [
      "Kushala Daora",
      "Rathian",
      "Rathalos",
      "Teostra"
    ]
  },
  {
    label: "De quelle race sont Hinoa et Minoto ?",
    imgSrc: imgJumelle,
    goodAnswer: 3,
    options: [
      "Femme",
      "Elfes",
      "Humaine",
      "Wyverienne"
    ]
  },
  {
    label: "Qui est le mystérieux monstre vivant au château de Shrade",
    imgSrc: imgDrgNoir,
    goodAnswer: 0,
    options: [
      "Fatalis",
      "Alatreon",
      "Black dragon",
      "Serpendelamorkitu"
    ]
  },
  {
    label: "Dans MH Iceborn, combien d'élement l'Alatreon utilise t-il ?",
    imgSrc: imgAlatreon,
    goodAnswer: 2,
    options: [
      "6",
      "3",
      "5",
      "oui"
    ]
  },
  {
    label: "Qui sont les compagnons poilus des chasseurs ?",
    imgSrc: imgPet,
    goodAnswer: 2,
    options: [
      "Chat et chien",
      "Felyne et palamutte",
      "Palico et palamute",
      "Je ne sais pas"
    ]
  },
  {
    label: "Qui sont les deux monstres icons des jeux Rise et Sunbreak ?",
    imgSrc: imgRise,
    goodAnswer: 3,
    options: [
      "Rathalos et Magnamalo",
      "Lunagaron et  Magnamalo",
      "Malzeno et Rathalos",
      "Malzeno et Magnamalo"
    ]
  },
  {
    
    label: "Dans MH Sunbreak, qui sont les trois seigneurs ?",
    imgSrc: imgTrois,
    goodAnswer: 1,
    options: [
      "Garangolme lunagaron Malezeno",
      "Garangolm lunagaron Malzeno",
      "Malzeno Shagaru-Magala Magnalo",
      "Malzeno Shagaru-Magala Magnamalo"
    ]
  },
  {
    label: "Qu'obtient-on si on fusionne de l'herbe et du miel ?",
    imgSrc: imgherbe,
    goodAnswer: 0,
    options: [
      "Potion",
      "Herbmiel",
      "un truc pas bon",
      "La réponse D"
    ]
  },
  {
    label: "A qui appartient cette empreinte ?",
    imgSrc: imgPaw,
    goodAnswer: 1,
    options: [
      "Rathalos",
      "Seregios",
      "un gros truc",
      "Zinogre"
    ]
  },
  {
    label: "De quel Yokaï est inspirer le Tetranodon ?",
    imgSrc: imgTetra,
    goodAnswer: 2,
    options: [
      "Kistune",
      "géant vert",
      "Kappa",
      "bakeneko"
    ]
  }
]
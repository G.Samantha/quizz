# Projet Quiz

Application web de quizz en TypesScript 

# Intro

Pour notre second projet, nous avons eu la consigne de créer un quiz en intégrant le JavaScript(avec TypesScript).

# JavaScript

J'ai utilisé JavaScript afin de rendre plus dynamique le quiz, avec notamment des timer ou un compteur de point.
J’ai intégré des images et des GIFS en créant un fichier spécifique global.d.ts afin d’importer tout cela dans le JS.
J’ai utilisé des objets afin de gérer mes questions et réponses.

#Liens

Lien du wireframe : https://www.figma.com/file/FZM2FsXPgI4M6xZiMPnh96/Wireframe-Quiz?node-id=0%3A1&t=1nP7PWE1mXiaqX5k-0

Lien du site : https://quizmonsterhunter.netlify.app/
